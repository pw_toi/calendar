'use strict';

Date.prototype.getDaysAmount = function() {
	return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
};


function Calendar(params) {
	this.init(params);
}

Calendar.prototype.init = function(params) {
	this.days = [
		'Понедельник',
		'Вторник',
		'Среда',
		'Четверг',
		'Пятница',
		'Суббота',
		'Воскресенье'
	];
	this.months = [
		'Январь',
		'Февраль',
		'Март',
		'Апрель',
		'Май',
		'Июнь',
		'Июль',
		'Август',
		'Сентябрь',
		'Октябрь',
		'Ноябрь',
		'Декабрь'
	];
	this.rootElement = document.getElementsByClassName('calendar-field')[0];
	this.data = [];
	if (params) {
		this.events = params.events;
		this.setDate(params.currentDate);
	} else {
		this.events = {};
		this.setDate();
	}
	this.setCurrentState();
	this.setEventListeners();
};

Calendar.prototype.setDate = function(date) {
	if (date) {
		this.currentDate = new Date(date);
		this.currentDay = null;
	} else {
		this.currentDate = new Date();
		this.currentDay = this.currentDate.getDate();
	}
	this.currentMonth = this.currentDate.getMonth();
	this.currentYear = this.currentDate.getFullYear();
	this.currentDate.setDate(1);

	this.toStorage();
};

Calendar.prototype.getDay = function() {
	var day = this.currentDate.getDay();
	if (day === 0) {
		day = 7;
	}
	return day - 1;
};

Calendar.prototype.getDayName = function(index) {
	return this.days[index];
};

Calendar.prototype.getMonthName = function(index) {
	return this.months[index];
};

Calendar.prototype.render = function() {
	var firstRow = true;
	var template = 	'<div class="calendar-cell{% current %}{% assigned %}" tabindex="1" id="{% id %}">' +
					'<span class="weekday">{% weekday %}</span>' +
					'<span class="date">{% date %}</span>' +
					'<div class="theme">{% theme %}</div>' +
					'<div class="members">{% members %}</div></div>';
	var str = '<div class="calendar-row">';
	var self = this;
	var currentDateId = self.getId(self.currentYear, self.currentMonth, self.currentDay);
	this.data.forEach(function(row, index) {
		var theme = '';
		var weekday = '';
		var current = '';
		var assigned = '';
        var members = '';

		if (firstRow) {
			weekday = self.getDayName(index) + ', ';
		}

		if (currentDateId === row.id) {
			current = ' current-day';
		}

		if (self.events[row.id]) {
			theme = self.events[row.id].theme;
			assigned = ' assigned';
            if (self.events[row.id].members) {
                members = self.events[row.id].members;
            }
		}

		str += template.replace('{% weekday %}', weekday)
			.replace('{% date %}', row.date)
			.replace('{% current %}', current)
			.replace('{% id %}', row.id)
			.replace('{% theme %}', theme)
			.replace('{% assigned %}', assigned)
			.replace('{% members %}', members);

		if (((index+1) % 7) === 0) {
			if (firstRow) {
				firstRow = false;
			}
			str += '</div><div class="calendar-row">';
		}
	});
	this.data = [];
	this.rootElement.innerHTML = str;
	this.addCellsListeners();
};

Calendar.prototype.setEventListeners = function() {
	var self = this;
	document.getElementById('prev-month').addEventListener('click', function() {
		self.switch(new Date(self.currentYear, self.currentMonth - 1));
	});
	document.getElementById('next-month').addEventListener('click', function() {
		self.switch(new Date(self.currentYear, self.currentMonth + 1));
	});
	document.getElementById('set-today').addEventListener('click', function() {
		self.switch();
	});
	document.getElementById('mini-form-close').addEventListener('click', function() {
		document.getElementById('add-event').checked = false;
	});
	document.getElementById('add-fast-event').addEventListener('click', function() {
		var input = document.getElementById('fast-event-data');
		var eventDataString = input.value;
		var splitString = eventDataString.split(',');
		var dateSplit = splitString[0].split(' ');
		var day = dateSplit[0];
		var month = '';
		dateSplit[1].replace(/([а-я]+)/, function(match) {
			var string = match.slice(0, -1);
			self.months.forEach(function(str, index) {
				if (string === str.toLowerCase()) {
					month = index;
				} else if (string === str.slice(0, -1).toLowerCase()) {
					month = index;
				}
			});
		});
		var id = self.getId(self.currentYear, month, day);
		var eventData = {
			'id'	: id,
			'theme' : splitString[1]
		};
		input.value = '';
		self.addEvent(eventData);
		document.getElementById('add-event').checked = false;
	});
	var form = document.getElementById('event-form');
	
	document.body.addEventListener('click', function(e) {
		e = e || window.event;
		if (!/calendar-cell/.test(e.target.className) && !/event-form/.test(e.target.className)) {
			form.style.display = 'none';
		}
	});
	document.getElementById('main-form-close').addEventListener('click', function() {
		form.style.display = 'none';
	});
	document.getElementById('save-event').addEventListener('click', function() {
		var form = this.parentNode;
		var eventData = {
			'id' : form.getAttribute('cell-id')
		};
		var error;
		[].slice.call(form.childNodes).forEach(function(el) {
			if (el.tagName === 'INPUT') {
				if (!el.value) {
					el.classList.add('empty-error');
					error = true;
				} else {
					el.classList.remove('empty-error');
				}
				eventData[el.name] = el.value;
			} else if (el.tagName === 'TEXTAREA') {
                eventData[el.name] = el.value;
            }
		});
		if (!error) {
			self.addEvent(eventData);
			form.style.display = 'none';
		}
	});
	document.getElementById('remove-event').addEventListener('click', function() {
		var form = this.parentNode;
		self.deleteEvent(form.getAttribute('cell-id'));
		self.clearForm(form);
	});
};

Calendar.prototype.fillData = function() {
	//количество дней в месяце
	var daysCnt = this.currentDate.getDaysAmount();
	//определяем начальный день месяца
	var monthStart = this.getDay();
	//если месяц начинается не с понедельника, то подгружаем последние дни предыдущего месяца
	if (monthStart !== 0) {
		var lastMonthDaysCnt = new Date(this.currentYear, this.currentMonth - 1).getDaysAmount();
		i = monthStart;
		while (i) {
			i--;
			this.data.push({
				'date' : lastMonthDaysCnt - i,
				'id'   : this.getId(this.currentYear, this.currentMonth - 1, lastMonthDaysCnt - i)
			});
		}
	}
	var allDays = monthStart + daysCnt;
	//подгружаем дни со следующего месяца
	for (var c = 1; c <= daysCnt; c++) {
		this.data.push({
			'date' : c,
			'id'   : this.getId(this.currentYear, this.currentMonth, c)
		});
	}
	var remainingDays;
	if (allDays < 35) {
		remainingDays = 35 - allDays;
	} else if (allDays > 35 && allDays < 42) {
		remainingDays = 42 - allDays;
	}
	for (var i = 1; i <= remainingDays; i++) {
		this.data.push({
			'date' : i,
			'id'   : this.getId(this.currentYear, this.currentMonth + 1, i)
		});
	}
};

Calendar.prototype.setCurrentState = function() {
	document.getElementById('current-month').innerHTML = this.getMonthName(this.currentMonth);
	document.getElementById('current-year').innerHTML = this.currentYear;
	while (this.rootElement.firstChild) {
		this.rootElement.removeChild(this.rootElement.firstChild);
	}
	this.fillData();
	this.render();
};

Calendar.prototype.switch = function(date) {
	this.setDate(date);
	this.setCurrentState();
};

Calendar.prototype.addEvent = function(eventData) {
	this.events[eventData.id] = eventData;
	var root = document.getElementById(eventData.id);
	if (root) {
		root.classList.add('assigned');
		[].slice.call(root.childNodes).map(function(node) {
			if (eventData[node.className]) {
				node.innerHTML = eventData[node.className];
			}
		});
	}
	this.toStorage();
};

Calendar.prototype.deleteEvent = function(eventId) {
	delete this.events[eventId];
	var root = document.getElementById(eventId);
	if (root) {
		root.classList.remove('assigned');
		[].slice.call(root.childNodes).map(function(node) {
			if (node.className === 'theme' || node.className === 'members') {
				node.innerHTML = '';
			}
		});
	}
	this.toStorage();
};

Calendar.prototype.clearForm = function(form) {
    [].slice.call(form.childNodes).map(function(el) {
        if (el.tagName === 'INPUT' || el.tagName === 'TEXTAREA') {
            el.value = '';
        }
    });
};

Calendar.prototype.getId = function() {
	if (arguments[1] < 0) {
		arguments[0] -= 1;
		arguments[1] = 0;
	}
	if (arguments[1] > 11) {
		arguments[0] += 1;
		arguments[1] = 0;
	}
	arguments.join = [].join;
	return arguments.join('');
};

Calendar.prototype.toStorage = function() {
    if (typeof Storage !== 'undefined') {
        var params = {
            'currentDate' : this.currentDate,
            'events'      : this.events
        };
        localStorage.setItem('calendar', JSON.stringify(params));
    }
};

Calendar.prototype.addCellsListeners = function() {
    var self = this;
    var form = document.getElementById('event-form');
    [].slice.call(document.getElementsByClassName('calendar-cell')).forEach(function(el) {
        el.addEventListener('click', function(e) {
            e = e || window.event;
            form.style.display = 'block';
            form.style.top = e.pageY - 65 + 'px';
            form.style.left = e.pageX + 15 + 'px';
            var id = this.id;
            form.setAttribute('cell-id', id);
            if (self.events[id]) {
                [].slice.call(form.childNodes).map(function(node) {
                    if (self.events[id][node.name]) {
                        node.value = self.events[id][node.name];
                    }
                });
            } else {
                self.clearForm(form);
            }
        });
    });
};


window.addEventListener('load', function() {
	if (typeof Storage !== 'undefined') {
        var params = JSON.parse(localStorage.getItem('calendar'));
        new Calendar(params);
    } else {
        new Calendar();
    }
});



